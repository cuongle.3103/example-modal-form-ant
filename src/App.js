import { ModalForm } from '@cuong.lv2/modal-form'
import { Button } from 'antd'
import { useState, useCallback } from 'react'
import moment from 'moment';

export function App() {
  const [open, setOpen] = useState(false)
  function disabledDate(current) {
    const weekStart = moment().startOf('week');
    const weekEnd = moment().endOf('week');
    return !(weekStart.isSameOrBefore(current) && weekEnd.isAfter(current));
  }
  let forms = [
    {
      title: 'Email',
      name: 'email',
      type: 'input',
      label: true,
      col: 6,
      rules: [
        {
          type: 'email',
          message: 'The input is not valid E-mail!',
        },
        {
          required: true,
          message: 'Please input your E-mail!',
        },
      ],
    },
    {
      title: 'SĐT',
      name: 'phone',
      label: true,
      type: 'number',
      col: 6,
      min: 10,
    },
    {
      title: 'Number',
      name: 'number',
      type: 'number',
      col: 6,
      // addonBefore: 'VNĐ',
      addonAfter: 'VNĐ',
      // disabled: true,
      min: 10,
      max: 1234567890
    },
    {
      title: 'Họ tên',
      name: 'name',
      type: 'input'
    },
    {
      title: 'Địa chỉ',
      label: true,
      name: 'address',
      type: 'text-area',
      rows: 4,
      maxLength: 100,
      col: 12,
      showCount: true
    },
    {
      title: 'Giới tính',
      name: 'sex',
      type: 'select',
      label: true,
      options: [{
        title: 'Nam',
        value: 'male'
      },
      {
        title: 'Nữ',
        value: 'female'
      },
      {
        title: 'BĐ',
        value: 'bd',
        disabled: true
      },
      ]
    },
    {
      title: 'Sở thích',
      name: 'like',
      type: 'multi-select',
      label: true,
      options: [
        {
          title: 'Đá bóng',
          value: 'bóng'
        },
        {
          title: 'Game',
          value: 'game'
        },
        {
          title: 'Karaoke',
          value: 'karaoke',
        },
        {
          title: 'Love',
          value: 'love',
        }
      ]
    },
    {
      title: 'Ngày sinh',
      name: 'birthday',
      type: 'date',
      disabledDate: disabledDate,
    },
    {
      title: 'Ngày sinh',
      name: 'birthday-time',
      type: 'date-time',
    },
    {
      title: 'Ngày sinh',
      name: 'birthday-range',
      type: 'date-range',
    },
    {
      title: 'Ngày sinh',
      name: 'birthday-time-range',
      type: 'date-time-range',
    },
    {
      title: 'Rate',
      name: 'rate',
      type: 'rate',
    },
    {
      title: 'Radio button',
      name: 'options',
      type: 'radio-button',
      label: true,
      col:12,
      options: [{
        title: 'Nam',
        value: 'male'
      },
      {
        title: 'Nữ',
        value: 'female'
      },
      {
        title: 'BĐ',
        value: 'bd',
        disabled: true
      },
      ]
    },
    {
      title: 'Checkbox',
      name: 'check',
      type: 'checkbox',
      label: true,
      col:12,
      options: [{
        title: 'Nam',
        value: 'male'
      },
      {
        title: 'Nữ',
        value: 'female'
      },
      {
        title: 'BĐ',
        value: 'bd',
        disabled: true
      },
      ]
    },
    {
      title: 'Upload',
      name: 'upload',
      type: 'upload',
      label: true,
      multiple: true,
      disabled: false,
      // maxCount:2,
      // textHelper: 'Upload file img',
      action: (file) =>{
        console.log("file", file)
      }
    }
  ]
  const handleSubmit = (e) => {
    console.log("e", e)
  };

  let data = {
    email: 'cuong.lv2@cmctelecom.vn',
    name: 'cuonglv',
    phone: '10000',
    address: 'HN',
    number: 123456789
  }


  return (
    <StyledApp>
      <Button onClick={() => setOpen(!open)}>Open</Button>
      <ModalForm
        title="Edit data"
        isOpen={open}
        handleCancel={() => setOpen(false)}
        forms={forms}
        handleSubmit={handleSubmit}
        formLayout="horizontal"
        data={data}
      />
      {/* <NxWelcome title="services-portal" /> */}
    </StyledApp>
  );
}

export default App;
